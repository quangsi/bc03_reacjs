import React, { Component } from "react";

export default class ChildComponent extends Component {
  render() {
    return (
      <div>
        Hello {this.props.helloTitle}
        <br />
        <button
          onClick={() => {
            this.props.handleOnclick("Tiến Linh");
          }}
          className="btn btn-warning"
        >
          Change name
        </button>
      </div>
    );
  }
}
