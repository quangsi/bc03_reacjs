import React, { Component } from "react";
import ChildComponent from "./Child.component";
import PropChildren from "./PropChildren";

// giao tiếp giữ các component với nhau ( dữ liệu )
export default class DemoProps extends Component {
  state = {
    username: "Alice",
  };

  handleChangeName = (name) => {
    this.setState({
      username: name,
    });
  };
  render() {
    return (
      <div>
        <ChildComponent
          helloTitle={this.state.username}
          handleOnclick={this.handleChangeName}
        />

        <h2>Demo PropChildren</h2>

        <PropChildren>Tôi yêu Việt Nam</PropChildren>
      </div>
    );
  }
}

// HOC HighOrderComponent

// propsChildren

// tái sử dụng code

// let a = "Alice";

// let b = a;
// console.log(b);
