import React, { Component } from "react";

export default class FormQuanLy extends Component {
  state = {
    infor: {
      taiKhoan: "alice",
      matKhau: "alice2022",
    },
  };

  handleChangeForm = (event) => {
    let value = event.target.value;
    let name = event.target.name;
    console.log({ value, name });
    // setState bất đồng bộ
    this.setState(
      {
        infor: { ...this.state.infor, [name]: value },
      },
      () => {
        console.log(this.state.infor);
      }
    );
  };

  handleSubmit = (e) => {
    console.log("submit", this.state.infor);
    e.preventDefault();
  };
  render() {
    return (
      <div>
        <form
          onSubmit={(e) => {
            this.handleSubmit(e);
          }}
        >
          <div class="form-group">
            <div>
              <label htmlFor="">Tài khoản</label>
              <input
                value={this.state.infor.taiKhoan}
                type="text"
                className="form-control"
                name="taiKhoan"
                placeholder="Tài khoản"
                onChange={(event) => {
                  this.handleChangeForm(event);
                }}
              />
            </div>
            <div class="form-group">
              <div>
                <label htmlFor="">Mật khẩu</label>
                <input
                  value={this.state.infor.matKhau}
                  type="text"
                  className="form-control"
                  name="matKhau"
                  placeholder="Mật khẩu"
                  onChange={(event) => {
                    this.handleChangeForm(event);
                  }}
                />
              </div>
            </div>
            <button className="btn btn-warning">Submit</button>
          </div>
        </form>
      </div>
    );
  }
}
