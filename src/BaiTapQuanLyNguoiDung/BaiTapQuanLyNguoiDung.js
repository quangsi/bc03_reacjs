import React, { Component } from "react";
import DanhSachQuanLy from "./DanhSachQuanLy";
import FormQuanLy from "./FormQuanLy";

export default class BaiTapQuanLyNguoiDung extends Component {
  render() {
    return (
      <div className="container text-left">
        <FormQuanLy />
        <DanhSachQuanLy />
      </div>
    );
  }
}
