import React, { Component } from "react";
import Header from "./Header";

export default class LifeCycle extends Component {
  state = {
    like: 0,
    share: 0,
    infor: {
      username: "Alice",
      age: 2,
    },
  };
  // tự  động dc chạy sau khi render chạy
  componentDidUpdate(prevProps) {
    console.log("componentDidUpdate");
  }
  
  componentDidMount() {
    console.log("Layout componentDidMount");
    // this.setState({ like: 100 });
  }
  render() {
    // this.setState({ like: 100 });

    console.log("layout render");
    return (
      <div>
        {/* {this.state.like < 5 && (
          <Header
            like={this.state.like}
            share={this.state.share}
            infor={this.state.infor}
          />
        )} */}
        {/* {this.state.like < 5 && <Header like={this.state.like} />} */}
        {/* like */}
        <div>
          <span className="display-4 text-success">{this.state.like}</span>
          <button
            className="btn btn-success"
            onClick={() => {
              this.setState({ like: this.state.like + 1 });
            }}
          >
            Plus like
          </button>
        </div>

        <div>
          <span className="display-4 text-primary">{this.state.share}</span>
          <button
            className="btn btn-primary"
            onClick={() => {
              this.setState({ share: this.state.share + 1 });
            }}
          >
            Plus share
          </button>
        </div>
      </div>
    );
  }
}
