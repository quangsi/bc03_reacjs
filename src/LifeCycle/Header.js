import React, { Component, PureComponent } from "react";
// PureComponent pass by value
export default class Header extends PureComponent {
  // constructor sẽ chạy đầu tiên
  //   constructor() {}
  componentDidMount() {
    console.log("Header componentDidMount");
    let totalTime = 300;
    this.myInterval = setInterval(() => {
      totalTime = totalTime - 1;
      console.log("totalTime", totalTime);
    }, 1000);
  }
  componentWillUnmount() {
    clearInterval(this.myInterval);
    console.log("HEADER DIE - UNMOUNT");
  }
  shouldComponentUpdate(nextProps, nextState) {
    console.log("nextProps", nextProps);
    console.log("oldProps", this.props);
    console.log("shouldComponentUpdate");
    // return true;
    if (nextProps.share !== this.props.share) {
      return true;
    }
  }
  UNSAFE_componentWillReceiveProps() {}
  render() {
    console.log("HEADER render");
    return (
      <div
        style={{ height: "max-content", minHeight: "100px" }}
        className="w-full bg-warning"
      >
        Header
        <p className="display-4 text-white">Like : {this.props.like}</p>
        <p className="display-4 text-white">Share : {this.props.share}</p>
        <p>Infor : {this.props.infor.username}</p>
      </div>
    );
  }
}
