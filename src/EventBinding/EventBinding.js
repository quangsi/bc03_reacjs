import React, { Component, Fragment } from "react";

export default class extends Component {
  handleSayHello = (username) => {
    console.log("Hello " + username);
  };
  render() {
    return (
      <Fragment>
        <button
          onClick={() => {
            this.handleSayHello("alice");
          }}
          className="btn btn-warning"
        >
          SayHello
        </button>
      </Fragment>
    );
  }
}
