import { combineReducers } from "redux";
import { numberReducer } from "./numberReducer";

export const rootReducerDemo = combineReducers({
  numberReducer,
});
//  key sẽ quản lý reducer => dùng key để lấy giá trị từ các key trong initialState
