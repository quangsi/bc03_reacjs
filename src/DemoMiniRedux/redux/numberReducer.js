import { TANG_SO_LUONG } from "./constants";

let initialState = {
  number: 0,
  total: 10,
};

export const numberReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case TANG_SO_LUONG: {
      console.log("yes");

      state.number += payload;
      // shallow copy
      return { ...state };
    }
    default:
      return state;
  }
};

// state management
