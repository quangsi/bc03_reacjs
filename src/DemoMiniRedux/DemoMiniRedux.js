import React, { Component } from "react";
import { connect } from "react-redux";
import { tangSoLuongAction } from "./redux/actions";
import { TANG_SO_LUONG } from "./redux/constants";

class DemoMiniRedux extends Component {
  render() {
    return (
      <div className="text-center display-4">
        <button
          onClick={this.props.hanlTangNumber}
          className="btn btn-success mx-2"
        >
          Tăng
        </button>
        {this.props.giaTri}
        <button className="btn btn-warning mx-2">Giảm</button>
      </div>
    );
  }
}
let mapStateToProps = (state) => {
  return {
    giaTri: state.numberReducer.number,
  };
};

let mapDispatchToProps = (dispatch) => {
  return {
    hanlTangNumber: () => {
      dispatch(tangSoLuongAction(100));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(DemoMiniRedux);
