import React, { Component } from "react";
import Cart from "./Cart";
import { dataShoeShop } from "./dataShoeShope";
import ProductItem from "./ProductItem";
import Productlist from "./Productlist";

export default class BaiTapShoeShop extends Component {
  state = {
    productList: dataShoeShop,
    gioHang: [],
    isOpenModal: false,
  };
  handleOpenModal = () => {
    this.setState({ isOpenModal: true });
  };
  handleCloseModal = () => {
    this.setState({ isOpenModal: false });
  };

  handleAddToCart = (sanPham) => {
    let cloneGioiHang = [...this.state.gioHang];

    let index = cloneGioiHang.findIndex((item) => {
      return item.id == sanPham.id;
    });
    if (index == -1) {
      // sản phẩm được thêm vào chưa có trong giỏ hàng
      let newSanPham = { ...sanPham, soLuong: 1 };
      cloneGioiHang.push(newSanPham);
    } else {
      // sản phẩm được thêm vào đã có trong giỏ hàng

      cloneGioiHang[index].soLuong++;
    }
    // cloneGioiHang.push(sanPham);
    this.setState({ gioHang: cloneGioiHang });
  };

  handleXoaSanPham = (idSanPham) => {
    // console.log("idSanPham", idSanPham);
    // console.log("this.state.gioHang", this.state.gioHang);
    let cloneGioiHang = [...this.state.gioHang];

    let index = cloneGioiHang.findIndex((item) => {
      return item.id == idSanPham;
    });

    // console.log({ index });
    if (index !== -1) {
      cloneGioiHang.splice(index, 1);

      this.setState({ gioHang: cloneGioiHang });
    }
  };

  handleTangGiamSoLuong = (idSanPham, giaTri) => {
    let cloneGioiHang = [...this.state.gioHang];
    let index = cloneGioiHang.findIndex((item) => {
      return item.id == idSanPham;
    });
    if (index !== -1) {
      cloneGioiHang[index].soLuong += giaTri;
    }
    // if (cloneGioiHang[index].soLuong == 0) {
    //   cloneGioiHang.splice(index, 1);
    // }

    cloneGioiHang[index].soLuong == 0 && this.handleOpenModal();
    // !cloneGioiHang[index].soLuong  && cloneGioiHang.splice(index, 1);

    this.setState({ gioHang: cloneGioiHang });
  };
  render() {
    return (
      <div>
        <Productlist
          handleThemSanPham={this.handleAddToCart}
          productList={this.state.productList}
        />
        <h4>
          Số số lượng sản phẩm trong giỏ hàng: {this.state.gioHang.length}{" "}
        </h4>
        {/* {this.state.gioHang.length > 0 ? (
          <Cart gioHang={this.state.gioHang} />
        ) : (
          " "
        )} */}
        {this.state.gioHang.length > 0 && (
          <Cart
            isOpenModal={this.state.isOpenModal}
            handleTangGiamSoLuong={this.handleTangGiamSoLuong}
            handleXoaSanPham={this.handleXoaSanPham}
            gioHang={this.state.gioHang}
          />
        )}
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
      </div>
    );
  }
}
// state management

// npm i redux react-redux
