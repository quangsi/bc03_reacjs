import React, { Component } from "react";
import ProductItem from "./ProductItem";

export default class Productlist extends Component {
  render() {
    return (
      <div className="container py-5">
        <div className="row">
          {this.props.productList.map((item, index) => {
            // console.log("dữ liệu", item);
            return (
              <ProductItem
                handleThemSanPham={this.props.handleThemSanPham}
                data={item}
                key={index}
              />
            );
          })}{" "}
        </div>
      </div>
    );
  }
}
