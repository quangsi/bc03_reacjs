import React, { Component } from "react";
import ModalConfirm from "./ModalConfirm";

export default class Cart extends Component {
  render() {
    return (
      <div className="container">
        <ModalConfirm isOpenModal={this.props.isOpenModal} />
        <table className="table">
          <thead>
            <td>ID</td>
            <td>Tên sản phảm</td>
            <td>Giá sản phẩm</td>
            <td>Số lượng</td>
            <td>Thao tác</td>
          </thead>
          <tbody>
            {this.props.gioHang.map((item) => {
              return (
                <tr>
                  <td>{item.id}</td>
                  <td>{item.name}</td>
                  <td>{item.price}</td>
                  <td>
                    <button
                      onClick={() => {
                        this.props.handleTangGiamSoLuong(item.id, 1);
                      }}
                      className="btn btn-success"
                    >
                      Tăng
                    </button>
                    <span className="mx-2">{item?.soLuong}</span>
                    <button
                      onClick={() => {
                        this.props.handleTangGiamSoLuong(item.id, -1);
                      }}
                      className="btn btn-secondary"
                    >
                      Giảm
                    </button>
                  </td>
                  <td>
                    <button
                      onClick={() => {
                        this.props.handleXoaSanPham(item.id);
                      }}
                      className="btn btn-danger"
                    >
                      Xoá
                    </button>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    );
  }
}
// {
//     "id": 7,
//     "name": "Adidas Ultraboost 4",
//     "alias": "adidas-ultraboost-4",
//     "price": 450,
//     "description": "The adidas Primeknit upper wraps the foot with a supportive fit that enhances movement.\r\n\r\n",
//     "shortDescription": "The midsole contains 20% more Boost for an amplified Boost feeling.\r\n\r\n",
//     "quantity": 854,
//     "image": "http://svcy3.myclass.vn/images/adidas-ultraboost-4.png"
// }
